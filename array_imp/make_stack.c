#include "stack.h"
/**
 * createStack - creates a stack of defined size
 * @size: size of the stack to create
 *
 * Return: pointer to memory allocated for the stack, or NULL
 */
stack *createStack(unsigned int size)
{
	stack *s;

	if (size < 1)
		return (NULL);
	s = (stack *)malloc(sizeof(stack));
	if (!s)
		return (NULL);
	s->top = -1;
	s->size = size;
	s->arr = (int *)malloc(sizeof(int) * size);
	if (!s->arr)
	{
		free(s);
		return (NULL);
	}

	return (s);
}
/**
 * freeStack - free memory allocated for a stack
 * @s: pointer to the stack
 */
void freeStack(stack *s)
{
	if (!s)
		return;
	free(s->arr);
	free(s);
}
