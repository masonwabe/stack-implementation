#include "stack.h"
/**
 * push - insert an element into the stack
 * @s: pointer to the stack
 * @n: element to insert
 */
void push(stack *s, int n)
{
	if (isFull(s))
		return;
	s->arr[++s->top] = n;
}

/**
 * pop - delete's an element from the stack
 * @s: pointer to the stack
 *
 * Return: the element removed from the stack
 */
int pop(stack *s)
{
	if (isEmpty(s))
		return (-1);
	return (s->arr[s->top--]);
}
/**
 * isEmpty - check if the stack is empty
 * @s: pointer to the stack
 *
 * Return: 1 if the stack is full otherwise 0
 */
int isEmpty(stack *s)
{
	return (s->top == -1);
}
/**
 * peek - returns the top element in the stack
 * @s: pointer to the stack
 *
 * Return: the value at the top of the stack
 */
int peek(stack *s)
{
	if (isEmpty(s))
		return (INT_MIN);
	return (s->arr[s->top]);
}
/**
 * isFull - check if the stack is full
 * @s: pointer to the stack
 *
 * Return: 1 if the stack if full otherwise 0
 */
int isFull(stack *s)
{
	return (s->top == s->size - 1);
}
