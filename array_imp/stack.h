#ifndef STACK
#define STACK

#include <stdlib.h>
#include <limits.h>
#include <stddef.h>

/**
 * struct stackk - define a struct for a stack implementation
 * @top: index of where we are in the stack
 * @size: size of the stack implemented
 * @arr: pointer to an array of elements in the stack
 */
typedef struct stackk
{
	int top;
	int size;
	int *arr;
} stack;

		/* stack operations */

/* creates a stack */
stack *createStack(unsigned int size);
/* insert an element into the stack */
void push(stack *, int);
/* delete's an element from the stack */
int pop(stack *);
/* check if the stack is empty */
int isEmpty(stack *);
/* returns the top element in the stack */
int peek(stack *);
/* check if the stack is full */
int isFull(stack *);

/* increase stack size */
stack *reStack(stack *);
/* free stack */
void freeStack(stack *);
#endif /* END STACK */
