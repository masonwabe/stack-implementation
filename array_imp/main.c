#include <stdio.h>
#include "stack.h"

/**
 * menu - generates menu options
 */
void menu(void)
{
	printf("\n******	MENU	******");
	printf("\nSelect an option below");
	printf("\n1. PUSH");
	printf("\n2. POP");
	printf("\n3. PEEK");
	printf("\n4. IS EMPTY");
	printf("\n5. IS FULL");
	printf("\n6. EXIT\n");
}
/**
 * main - allows a user to interact with a stack
 *
 * Return: always 0.
 */
int main(void)
{
	int size, sel, val;
	stack *s;

	printf("Enter size of the stack: ");
	scanf("%d", &size);
	s = createStack(size);
	do {
		menu();
		scanf("%d", &sel);
		switch (sel)
		{
			case 1:
				printf("\nenter value: ");
				scanf("%d", &val);
				push(s, val);
				break;
			case 2:
				printf("\nremoved: %d", pop(s));
				break;
			case 3:
				printf("\n%d", peek(s));
				break;
			case 4:
				isEmpty(s) ? printf("\nempty!") : printf("\nnot empty");
				break;
			case 5:
				isFull(s) ? printf("\nfull!") : printf("\nnot full");
				break;
			case 6:
				freeStack(s);
				exit(0);
			default:
				printf("\nentered wrong option");
				break;
		}
	} while (1);
	freeStack(s);
	return (0);
}
