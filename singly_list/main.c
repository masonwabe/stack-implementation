#include <stdio.h>
#include "stack.h"

/**
 * menu - generates menu options
 */
void menu(void)
{
	printf("\n******	MENU	******");
	printf("\nSelect an option below");
	printf("\n1. PUSH");
	printf("\n2. POP");
	printf("\n3. PEEK");
	printf("\n4. IS EMPTY");
	printf("\n5. VIEW STACK");
	printf("\n6. EXIT\n");
}
/**
 * main - allows a user to interact with a stack
 *
 * Return: always 0.
 */
int main(void)
{
	node *head;
	int sel, val;

	head = NULL;
	do {
		menu();
		scanf("%d", &sel);
		switch (sel)
		{
			case 1:
				printf("\nenter value: ");
				scanf("%d", &val);
				push(&head, val);
				break;
			case 2:
				printf("\nremoved: %d", pop(&head));
				break;
			case 3:
				printf("%d", peek(head));
				break;
			case 4:
				isEmpty(head) ? printf("\nnot empty") : printf("\nempty!");
				break;
			case 5:
				display(head);
				break;
			case 6:
				freeStack(head);
				(void) head;
				exit(0);
			default:
				printf("\nentered wrong option");
				break;
		}
	} while (1);
	freeStack(head);
	(void) head;
	return (0);
}
