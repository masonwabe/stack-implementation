#ifndef STACK
#define STACK

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stddef.h>

/**
 * struct stackk - define a struct for a stack implementation
 * @data: data for the node
 * @next: pointer to the next node in the stack
 */
typedef struct stack
{
	int data;
	struct stack *next;
}node;

/* define head node */
extern node *head;

		/* stack operations */

/* insert an element into the stack */
void push(node **, int);
/* delete's an element from the stack */
int pop(node **);
/* check if the stack is empty */
int isEmpty(node *);
/* returns the top element in the stack */
int peek(node *);

/* free stack */
void freeStack(node *);
/* displays the nodes in the stack */
void display(node *);
#endif /* END STACK */
