#include "stack.h"
/**
 * push - insert an element into the stack
 * @n: data to insert
 */
void push(node *(*head), int n)
{
	node *tmp;

	tmp = (node *)malloc(sizeof(node));
	if (!tmp)
		return;
	tmp->data = n;
	if ((*head) == NULL)
		tmp->next = NULL;
	else
		tmp->next = (*head);
	(*head) = tmp;
}
/**
 * pop - delete's an element from the stack
 *
 * Return: the element removed from the stack
 */
int pop(node **head)
{
	int data_t;
	node *node_t;

	if (!isEmpty((*head)))
		return (INT_MIN);
	node_t = (*head);
	data_t = (*head)->data;
	*head = (*head)->next;
	free(node_t);
	return (data_t);
}
/**
 * isEmpty - check if the stack is empty
 *
 * Return: 0 if the stack is empty otherwise 1
 */
int isEmpty(node (*head))
{
	if (head == NULL)
		return (0);

	return (1);
}
/**
 * peek - returns the top element in the stack
 *
 * Return: the value at the top of the stack
 */
int peek(node (*head))
{
	if (!isEmpty(head))
		return (INT_MIN);
	return (head->data);
}
/**
 * freeStack - free memory allocated for a stack
 */
void freeStack(node (*head))
{
	node *node_t;

	if (!head)
		return;
	for (; head != NULL;)
	{
		node_t = head;
		head = head->next;
		free(node_t);
	}
}
