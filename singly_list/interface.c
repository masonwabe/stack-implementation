#include "stack.h"
/**
 * display - shows's all the nodes in the stack
 */
void display(node *head)
{
	node *node_t;

	node_t = head;
	printf("- - - - Stack View - - - -\n");
	for (; node_t != NULL;)
	{
		printf("\t%d\n", node_t->data);
		node_t = node_t->next;
	}
}
